Role Name
=========

dsecassandra


Role Definition
=========

This role is responsible for completing pre requirements in order to install and start dse cassandra database. It can be used for the following reasons

1. Configure kernel recomended kernel parameters
2. Install pre-packages for OS
3. Install DSE packages
4. Configure OS limits for DSE Cassandra
5. Configure session limits for OS

Role Dependicies
=========

Supported Operating Systems

* CentOS 7.X

Requirements
=========

* ansible>=2.9.3

Installation
=========

``` bash
git clone https://gitlab.com/demirhuseyinn.94/dse-cassandrabase.git
``` 

``` bash
ansible-galaxy install -r requirements.yml
``` 

Example Playbook
=========

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
``` yaml
---
- hosts: localhost
  connection: local
  roles:
    - dsecassandra
```

Author Information
=========

Hüseyin Demir
